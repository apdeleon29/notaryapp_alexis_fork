"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var app_component_1 = require("./app.component");
var annonces_form_component_1 = require("../app/Admin/Annonces/annonces.form.component");
var messages_form_component_1 = require("../app/Admin/Messages/messages.form.component");
var notes_form_component_1 = require("../app/Admin/Notes/notes.form.component");
var envoyer_form_component_1 = require("../app/Admin/Envoyer/envoyer.form.component");
var client_form_component_1 = require("../app/Admin/Client/client.form.component");
var documents_form_component_1 = require("../app/Admin/TousLesDocument/documents.form.component");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [platform_browser_1.BrowserModule],
            declarations: [app_component_1.AppComponent,
                client_form_component_1.ClientComponent,
                annonces_form_component_1.AnnoncesComponent,
                messages_form_component_1.MessagesComponent,
                notes_form_component_1.NotesComponent,
                envoyer_form_component_1.EnvoyerComponent,
                documents_form_component_1.DocumentsComponent],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map