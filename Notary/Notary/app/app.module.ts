import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

import { AnnoncesComponent } from '../app/Admin/Annonces/annonces.form.component';
import { MessagesComponent } from '../app/Admin/Messages/messages.form.component';
import { NotesComponent } from '../app/Admin/Notes/notes.form.component';
import { EnvoyerComponent } from '../app/Admin/Envoyer/envoyer.form.component';
import { ClientComponent } from '../app/Admin/Client/client.form.component';
import { DocumentsComponent } from '../app/Admin/TousLesDocument/documents.form.component';

@NgModule({ 
  imports:      [ BrowserModule ],
    declarations: [AppComponent,
        ClientComponent,
        AnnoncesComponent,
        MessagesComponent,
        NotesComponent,
        EnvoyerComponent,
        DocumentsComponent],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
